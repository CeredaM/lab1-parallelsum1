# SOMMA PARALLELA #

Scrivere un programma multithread in Java che dato un array di 50 valori numerici ne esegue la somma in modo parallelo.
Creare  10  thread  e assegnare ad ogni thread una partizione dell’array (5 valori).
Ogni thread esegue la somma dei propri valori e salva il risultato internamente.
Il thread main deve sincronizzarsi sulla terminazione (join) dei thread figli dopodiche' somma i risultati parziali e li stampa su stdout.